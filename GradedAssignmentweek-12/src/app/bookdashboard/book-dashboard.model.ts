export class BookModel{
    id : number = 0;
    bookName: string = '';
    author : string = '';
    year : string = '';
}