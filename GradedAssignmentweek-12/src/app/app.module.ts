import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { CustomerDashboardComponent } from './customer-dashboard/customer-dashboard.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BookdashboardComponent } from './bookdashboard/bookdashboard.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { ShowbookComponent } from './showbook/showbook.component';

@NgModule({
  declarations: [
    AppComponent,
   
    CustomerDashboardComponent,
   
    BookdashboardComponent,
   
    LoginComponent,
   
    SignupComponent,
   
    ShowbookComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
