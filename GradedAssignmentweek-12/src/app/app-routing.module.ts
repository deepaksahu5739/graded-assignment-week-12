import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookdashboardComponent } from './bookdashboard/bookdashboard.component';
import { CustomerDashboardComponent } from './customer-dashboard/customer-dashboard.component';

import { LoginComponent } from './login/login.component';
import { ShowbookComponent } from './showbook/showbook.component';
import { SignupComponent } from './signup/signup.component';
const routes: Routes = [{path: '',redirectTo:'login',pathMatch:'full'},
{path: 'login',component:LoginComponent},
{path: 'signup',component:SignupComponent},
{path: 'dashboard',component:CustomerDashboardComponent},
{ path: 'bookdashboard',component:BookdashboardComponent},
{ path: 'viewbooks' ,component:ShowbookComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
